function greet() {
  var i = 1;
  for (; i <= 10; i++) {
    setTimeout((function (i) {
        return function() {
            console.log("Hi " + i);
        }
    })(i), i * 1000);
  }
}


function log(i) {
    return function() {
        console.log("Hi " + i);
    }
}

greet();
