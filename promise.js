function asyncAdd(x, y, cb) {
  setTimeout(function add() {
    cb(x + y);
  }, Math.random() * 20 + 20);
}

function asyncInc(x, cb) {
  setTimeout(function double() {
    cb(x + 1);
  }, Math.random() * 20 + 20);
}

function asyncDouble(x, cb) {
  setTimeout(function double() {
    cb(2 * x);
  }, Math.random() * 20 + 20);
}

const promiseAdd = (x, y) =>
  new Promise(function (resolve, reject) {
    asyncAdd(x, y, resolve);
  });

const promiseFail = () => new Promise(function (_resolve, reject) {
    reject("Error");
  });

const promiseFail2 = () =>
  new Promise(function (_resolve, reject) {
    throw "Error";
  });

const promiseInc = (x) =>
  new Promise(function (resolve, reject) {
    asyncInc(x, resolve);
  });

const promiseDouble = (x) =>
  new Promise(function (resolve, reject) {
    asyncDouble(x, resolve);
  });

promiseAdd(4, 5).then(promiseInc).then(console.log);

//compose, map, filter, reduce

async function test() {
  let result = await promiseAdd(4, 5);
  result = await promiseInc(result);
  result = await promiseInc(result);
  result = await promiseDouble(result);
  result = await promiseInc(result);
  return result;
}

test().then(console.log);

async function test2() {
  var fs = [promiseDouble, promiseInc];
  var x = 4;
  for (f of fs) {
    x = await f(x);
  }

  return x;
}

test2().then(console.log);

async function test3() {
  return 5555;
}

test3()
  .then((result) => result * 2)
  .then(console.log);

Promise.resolve(45).then(console.log);



function promiseCompose(g ,f) {
  return function(x) {
    return f(x).then(g)
  }
}

promiseCompose(promiseInc, promiseDouble)(4).then(res => console.log("promiseCompose", res));


Promise.all([promiseAdd(3,4), promiseInc(8), Promise.resolve(1)]).then(result => console.log(result));

Promise.race([promiseAdd(3, 4), promiseInc(8), Promise.resolve(1)]).then(
  (result) => console.log(">>>", result)
);


promiseInc(6).then(_ => promiseFail()).then(_ => console.log("ok")).catch(error => console.log("error"));


Promise.all([
  promiseAdd(3, 4),
  promiseInc(8),
  Promise.resolve(1),
  Promise.reject("error")//.catch((_) => 1000),
])
  .then((result) => console.log(result))
  .catch(error => console.log("hay un error"));


function withError() {
  try {
    throw new Error("Error")
  } catch(err) {
    console.log("Err:", err.message)

  }
}

withError();

function withAsyncError() {
  try {
    setTimeout(function() {
      throw new Error("Error");
    }, 0)
  } catch (err) {
    console.log("Async Err:", err.message);
  }
}

// withAsyncError();

async function withPromiseError() {
  try {
    //await Promise.reject("error");
    await promiseFail2();
  } catch (err){
  console.log("Promise Err:", err);
  }
}; 

withPromiseError();
