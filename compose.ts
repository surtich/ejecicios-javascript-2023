function compose<T, R, S>(f: (y: S) => T, g: (x: R) => S): (x: R) => T {
  return function (x: R) {
    return f(g(x));
  };
}
