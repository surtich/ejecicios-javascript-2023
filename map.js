const { reduce } = require("./reduce");

var double = (number) => number * 2;

var inc = (number) => number + 1;

function doubleAfterInc(number) {
  return double(inc(number));
}

function incAfterDoubleAfterInc(x) {
  return inc(doubleAfterInc(x));
}

function initialize(word) {
  var [initial, ...res] = word;
  initial = initial == undefined ? "" : initial.toUpperCase();
  res = res.join("");
  return initial + res;
}

var negate = (p) => !p;

function map(f, xs) {
  var ys = [];
  for (var x of xs) {
    ys.push(f(x));
  }
  return ys;
}

function curryMap(f) {
  return function (xs) {
    var ys = [];
    for (var x of xs) {
      ys.push(f(x));
    }
    return ys;
  };
}

function mapRecu(f, xs) {
  if (xs.length == 0) {
    return [];
  } else {
    var [head, ...tail] = xs;
    return [f(head), ...mapRecu(f, tail)];
  }
}

function mapRecu2(f, xs) {
  function _mapRecu2(f, xs, result) {
    if (xs.length == 0) {
      return result;
    } else {
      var [head, ...tail] = xs;
      return _mapRecu2(f, tail, [...result, f(head)]);
    }
  }
  return _mapRecu2(f, xs, []);
}

function mapWithReduce(f, xs) {
  return reduce(
    function (acc, x) {
      return [...acc, f(x)];
    },
    [],
    xs
  );
}

module.exports = {
  map,
  mapRecu,
  mapRecu2,
  mapWithReduce,
  curryMap,
  double,
  inc,
  initialize,
  negate,
};
