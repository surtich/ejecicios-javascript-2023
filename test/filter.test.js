var {filter, isOdd, greaterThanTwo, startsWithVowel} = require("../filter");


describe("Pruebas de filter", function() {
    test('isOdd [1,2,5] es [1,5]', function() {
        expect(filter(isOdd, [1,2,5])).toEqual([1,5]);
    });

    test('isOdd ["Pepe", "Pep", "Pe"] es ["Pepe", "Pep"]', function() {
        expect(filter(greaterThanTwo, ["Pepe", "Pep", "Pe"])).toEqual(["Pepe", "Pep"]);
    });
    
    test('el filter de un array vacío es []', function() {
        expect(filter(isOdd, [])).toEqual([]);
    });

});



describe("Pruebas de startsWithVowel", function() {
    it('"" deberia ser falso', function() {
        expect(startsWithVowel("")).toEqual(false);
    });
    it('"abc" deberia ser verdadero', function() {
        expect(startsWithVowel("abc")).toEqual(true);
    });
    it('"Abc" deberia ser verdadero', function() {
        expect(startsWithVowel("Abc")).toEqual(true);
    });
    it('"bc" deberia ser false', function() {
        expect(startsWithVowel("bc")).toEqual(false);
    });

});

