var {
  map,
  mapRecu,
  mapRecu2,
  mapWithReduce,
  curryMap,
  negate,
  inc,
  double,
  initialize,
} = require("../map");

var { compose } = require("../compose");

describe("Pruebas de map", function () {
  test("el identity de [1,2,5] es [1,2,3]", function () {
    expect(map((x) => x, [1, 2, 5])).toEqual([1, 2, 5]);
  });

  test("el double de [1,2,5] es [2,4,10]", function () {
    expect(map(double, [1, 2, 5])).toEqual([2, 4, 10]);
  });

  test("el double de [1,2,5] es [2,4,10]", function () {
    expect(mapRecu(double, [1, 2, 5])).toEqual([2, 4, 10]);
  });

  test("el double de [1,2,5] es [2,4,10]", function () {
    expect(mapRecu2(double, [1, 2, 5])).toEqual([2, 4, 10]);
  });

  test("el double de [1,2,5] es [2,4,10]", function () {
    expect(mapWithReduce(double, [1, 2, 5])).toEqual([2, 4, 10]);
  });

  test("el double de [1,2,5] es [2,4,10]", function () {
    expect([1, 2, 5].map(double)).toEqual([2, 4, 10]);
  });

  test("el map de un array vacío es []", function () {
    expect(map((x) => x, [])).toEqual([]);
  });

  test("el incremento de [1,2,5] es [2,3,6]", function () {
    expect(map(inc, [1, 2, 5])).toEqual([2, 3, 6]);
  });

  test("el negate de [true, false, false] es [false, true, true]", function () {
    expect(map(negate, [true, false, false])).toEqual([false, true, true]);
  });

  test("el doble del incremento de [1,2,5] es [4,6,12]", function () {
    expect(map(double, map(inc, [1, 2, 5]))).toEqual([4, 6, 12]);
  });

  test("la composción del doble del incremento de [1,2,5] es [4,6,12]", function () {
    expect(map(compose(double, inc), [1, 2, 5])).toEqual([4, 6, 12]);
  });

  test("con curryMap el double de [1,2,5] es [2,4,10]", function () {
    expect(curryMap(double)([1, 2, 5])).toEqual([2, 4, 10]);
  });
});

describe("Pruebas de initialize", function () {
  test('La capitalización de "pepe", "Pepe"', function () {
    expect(initialize("pepe")).toEqual("Pepe");
  });

  test('La capitalización de "p" es "P"', function () {
    expect(initialize("p")).toEqual("P");
  });

  test('La capitalización de "P" es "P"', function () {
    expect(initialize("P")).toEqual("P");
  });

  test('La capitalización de "1p" es "1p"', function () {
    expect(initialize("1p")).toEqual("1p");
  });

  test('La capitalización de "" es ""', function () {
    expect(initialize("")).toEqual("");
  });

  test("el inc de [[1,2,3], [4,5,6]] -> [[2,3,4],[5,6,7]]", function () {
    expect(
      map(curryMap(inc), [
        [1, 2, 3],
        [4, 5, 6],
      ])
    ).toEqual([
      [2, 3, 4],
      [5, 6, 7],
    ]);
  });
});
