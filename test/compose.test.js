var { compose, pipe } = require("../compose");
var {
  filter,
  curryFilter,
  isOdd,
  greaterThanTwo,
  startsWithVowel,
} = require("../filter");

var { map, curryMap, inc, double } = require("../map");

describe("Pruebas de compose", function () {
  test("la composición de (double, inc)(7) es 16", function () {
    expect(compose(double, inc)(7)).toEqual(16);
  });

  test("la composición inversa de (double, inc)(7) es 15", function () {
    expect(pipe(double, inc)(7)).toEqual(15);
  });

  test("test de filter con map", function () {
    expect(map(inc, filter(isOdd, [1, 2, 4, 5, 6]))).toEqual([2, 6]);
  });

  test("test map con compose", function () {
    expect(map(compose(isOdd, inc), [1, 2, 3])).toEqual([false, true, false]);
  });

  test("test de curryFilter con curryMap", function () {
    expect(compose(curryMap(double), curryFilter(isOdd))([1, 2, 3])).toEqual([
      2, 6,
    ]);
  });
});
