const {
  reduce,
  reduceRecu,
  reduceRecu2,
  add,
  max,
  uniqueChars,
  consonants,
  moreUniqueConsonatsThan,
  acronymize,
} = require("../reduce");

describe("Pruebas de reduce", function () {
  it("should be 8 the sum of [1,3,4]", function () {
    expect(reduce(add, 0, [1, 3, 4])).toBe(8);
  });

  it("should be 0 the sum of []", function () {
    expect(reduce(add, 0, [])).toBe(0);
  });

  it("should be 8 the max of [1,8,4]", function () {
    expect(reduce(max, 0, [1, 8, 4])).toBe(8);
  });

  it("should be 0 the max of []", function () {
    expect(reduce(max, 0, [])).toBe(0);
  });

  test("of moreUniqueConsonatsThan", function () {
    expect(
      reduce(moreUniqueConsonatsThan, 0, [
        "pepeluis",
        "rodrigo",
        "ppppppppllllll",
      ])
    ).toBe(3);
  });

  test("the acronym of 'organización  de las naciones unidads' es", function () {
    expect(reduce(acronymize, "", ["organización", "naciones", "unidas"])).toBe(
      "ONU"
    );
  });

  it("should be 8 the sum of [1,3,4]", function () {
    expect(reduceRecu(add, 0, [1, 3, 4])).toBe(8);
  });

  it("should be 8 the sum of [1,3,4]", function () {
    expect(reduceRecu2(add, 0, [1, 3, 4])).toBe(8);
  });

  test("the acronym of 'organización  de las naciones unidads' es", function () {
    expect(
      reduceRecu(acronymize, "", ["organización", "naciones", "unidas"])
    ).toBe("ONU");
  });

  test("the acronym of 'organización  de las naciones unidads' es", function () {
    expect(
      reduceRecu2(acronymize, "", ["organización", "naciones", "unidas"])
    ).toBe("ONU");
  });
});

describe("Pruebas de uniqueChars", function () {
  test("'pePeLuis have p,l,s consonats", function () {
    expect(uniqueChars(consonants, "pePeLuis")).toEqual(["p", "l", "s"]);
  });
});
