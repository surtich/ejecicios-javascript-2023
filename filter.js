var isOdd = (x) => x % 2 == 1;

var greaterThanTwo = (x) => x.length > 2;

var startsWithVowel = (word) => /^[aeiou]/i.test(word);
var containsVowel = (word) => /[aeiou]/i.test(word);

function filter(p, xs) {
  var ys = [];
  for (var x of xs) {
    if (p(x)) {
      ys.push(x);
    }
  }
  return ys;
}

function curryFilter(p) {
  return function (xs) {
    var ys = [];
    for (var x of xs) {
      if (p(x)) {
        ys.push(x);
      }
    }
    return ys;
  };
}

module.exports = {
  filter,
  curryFilter,
  isOdd,
  greaterThanTwo,
  startsWithVowel,
  containsVowel,
};
