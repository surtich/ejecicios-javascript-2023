function compose(f, g) {
  return function (x) {
    return f(g(x));
  };
}

function pipe(f, g) {
  return function (x) {
    return g(f(x));
  };
}

module.exports = {
  compose,
  pipe,
};
