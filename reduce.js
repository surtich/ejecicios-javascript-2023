function reduce(f, init, xs) {
  var acc = init;
  for (var x of xs) {
    acc = f(acc, x);
  }
  return acc;
}

function reduceRecu(f, acc, xs) {
  if (xs.length === 0) {
    return acc;
  } else {
    var [head, ...tail] = xs;
    acc = f(acc, head);
    return reduceRecu(f, acc, tail);
  }
}

function reduceRecu2(f, acc, xs) {
  if (xs.length === 0) {
    return acc;
  } else {
    var last = xs[xs.length - 1];
    var header = xs.slice(0, xs.length - 1);
    return f(reduceRecu2(f, acc, header), last);
  }
}

var add = (x, y) => x + y;

var max = (x, y) => (x > y ? x : y);

var consonants = "bcdfghjklmnpqrstvwxyz";

var uniqueChars = (chars, word) =>
  Array.from(new Set(word.toLowerCase().match(new RegExp(`[${chars}]`, "g"))));

var moreUniqueConsonatsThan = (maxLength, word) =>
  max(maxLength, uniqueChars(consonants, word).length);

var acronymize = (acronym, word) =>
  word.length === 0 ? "" : acronym + word[0].toUpperCase();

module.exports = {
  reduce,
  reduceRecu,
  reduceRecu2,
  add,
  max,
  uniqueChars,
  consonants,
  moreUniqueConsonatsThan,
  acronymize,
};
