function asyncAdd(x, y, cb) {
  return setTimeout(function add() {
    cb(x + y);
  }, Math.random() * 20 + 20);
}

function asyncInc(x, cb) {
  return setTimeout(function double() {
    cb(x + 1);
  }, Math.random() * 20 + 20);
}

function asyncDouble(x, cb) {
  return setTimeout(function double() {
    cb(2 * x);
  }, Math.random() * 20 + 20);
}

// asyncDouble(3, function (double) {
//   asyncInc(double, function (inc) {
//     console.log(inc);
//   });
// });

function asyncCompose(g, f) {
  return function (x, cb) {
    f(x, function (result) {
      g(result, cb);
    });
  };
}

var h = asyncCompose(asyncInc, asyncDouble);

//h(3, console.log); // 7
// asyncMap, asyncFilter y asyncReduce

function asyncId(x, cb) {
  return cb(x);
}

function asyncMultiCompose(...fs) {
  const gs = fs.reverse();
  let compose = asyncId;
  for (let g of gs) {
    compose = asyncCompose(g, compose);
  }
  return compose;
}

// asyncMultiCompose()(4, console.log);
// asyncMultiCompose(asyncInc)(4, console.log);
// asyncMultiCompose(asyncInc, asyncDouble)(4, console.log);
// asyncMultiCompose(asyncInc, asyncInc, asyncDouble)(4, console.log);

class AsyncComposer {
  constructor(f) {
    this.f = f;
  }

  then(g) {
    return new AsyncComposer(asyncCompose(g, this.f));
  }

  exec(x, cb) {
    this.f(x, cb);
  }
}

new AsyncComposer(asyncInc).then(asyncDouble).exec(4, console.log);
new AsyncComposer(asyncInc)
  .then(asyncInc)
  .then(asyncDouble)
  .exec(4, console.log);

function asyncMap(f, xs, cb) {
  const ys = [];
  let count = 0;
  for (let i in xs) {
    f(xs[i], (y) => {
      ys[i] = y;
      count++;
      if (count === xs.length) {
        cb(ys);
      }
    });
  }
}

asyncMap(asyncInc, [1, 2, 3, 4, 5], console.log);

//function asyncMap2(fs, x, callback);
// asyncMap2([asynInc, asyncDouble], 5, console.log); //[[6, 10]

const q = new AsyncComposer(asyncInc).then(asyncInc).then(asyncDouble);

q.exec(4, console.log);
q.exec(14, console.log);


function asyncGreatThan10(x, cb) {
  return setTimeout(function double() {
    cb(x > 10);
  }, Math.random() * 20 + 20);
}
function asyncFilter(p, xs, cb) {
  let ys = [];
  let cont = 0;
  for (let i in xs) {
    p(xs[i], (y) => {
      if (y) {
        ys[i] = xs[i];
      }
      cont++;
      if (cont === xs.length) {
        ys = ys.filter((_y, i) => i in ys);
        cb(ys);
      }
    });
  }
}

asyncFilter(asyncGreatThan10, [5, 10, 15, 20, 25], (value) => {
  console.log(value);
});

function asyncReduce(f, initialValue, xs, callback) {
  if (xs.length === 0) {
    return callback(initialValue);
  }
  asyncReduce(f, initialValue, xs.slice(1), (res) => f(xs[0], res, callback))
}

function asyncMax(x, y, callback) {
  return setTimeout(() => callback(Math.max(x, y)));
}

function asyncConcat(x, y, callback) {  
  return setTimeout(() => callback(x + y));
}

asyncReduce(asyncMax, 0, [1, 3, 2, 4, 5, 8, 7, 9, 6], (max) => console.log("Max: ", max));
asyncReduce(asyncConcat, "", ["Hola", " ", "pepe"], (xs) =>
  console.log("Concat: ", xs)
);

function asyncMap2(fs, value, cb) {
  const ys = [];
  let count = 0;
  for (let i in fs) {
    fs[i](value, (newValue) => {
      ys[i] = newValue;
      count++;
      if (count === fs.length) {
        cb(ys);
      }
    });
  }
}

function asyncMultiMap(fs, xs, cb) {
  const ys = [];
  let count = 0;
  for (let i in fs) {
    ys[i] = [];
    for (let j in xs) {
      fs[i](xs[j], (y) => {
        ys[i][j] = y;
        count++;
        if (count === fs.length * xs.length) {
          cb(ys);
        }
      });
    }
  }
}

asyncMap(asyncInc, [1, 2, 3, 4, 5], (value) => {
  console.log(value);
});
asyncMap2([asyncInc, asyncDouble], 3, console.log);
asyncMultiMap([asyncInc, asyncDouble], [1, 2, 3], console.log);

