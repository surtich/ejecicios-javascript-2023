// https://www.codewars.com/kata/56786a687e9a88d1cf00005d/train/javascript

function validateWord(s) {
  var freq = {};
  for (s of s.toLowerCase().split("")) {
    if (freq[s]) {
      freq[s]++;
    } else {
      freq[s] = 1;
    }
  }

  return new Set(Object.values(freq)).size == 1;
}
